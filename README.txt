INTRODUCTION
------------

Exposes the xPaw PHP-Minecraft-Query library public methods 
within MinecraftQuery.php and MinecraftPing.php. Supports standard Minecraft 
ping protocol, both pre and post 1.7, as well as the Gamespy4 protocol.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/minecraft_query

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/minecraft_query

REQUIREMENTS
------------

This module requires the following modules:

 * Libraries (https://drupal.org/project/libraries)
 * x Autoload (https://drupal.org/project/xautoload)
 
This module requires the following libraries:

 * xPaw/PHP-Minecraft-Query (https://github.com/xPaw/PHP-Minecraft-Query)

INSTALLATION
------------
 
 * Install the xPaw/PHP-Minecraft-Query library. See:
   https://www.drupal.org/node/1440066
   
 * Install dependencies as you would normally install contributed Drupal
   modules. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 
 * No configurations available. This is a library.
